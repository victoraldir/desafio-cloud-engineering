#!/bin/bash
check_status() {
  if [[ $1 != 0 ]]; then
    echo
    echo $2
    exit $1
  fi
}

MINIKUBE_IP=$(minikube ip)
HOSTS_ENTRY="challenge-app.info"

# Delete solutions's folder if it exists
rm -rf desafio-cloud-engineering

# Clone repository
git clone https://victoraldir@bitbucket.org/victoraldir/desafio-cloud-engineering.git
check_status $? 'Error when cloning repository'

# Get into app folder to build image locally
cd desafio-cloud-engineering/Kubernetes/app/
check_status $? 'Error when accessing desafio-cloud-engineering folder'

# Set docker to point to minikube
eval $(minikube docker-env)
check_status $? 'Error when setting docker to point to minikube'

# Enable the NGINX Ingress controller to minikube
minikube addons enable ingress
check_status $? 'Error when enabling NGINX Ingress controller'

# Build image tagging it as 'challengeapp/node-web-app'
docker build -t challengeapp/node-web-app .
check_status $? "Error when building challenge's frontend images"

# Get kubectl in minikube's context
kubectl config use-context minikube
check_status $? "Error when getting kubectl in minikube's context"

# Delete any existing deployment named as challenge-app
kubectl delete deployment challenge-app

# Delete any existing deployment named as challenge-app
kubectl delete deployment challenge-app

# Delete any existing service named as challenge-app
kubectl delete svc challenge-app

# Delete any existing ingress named as challenge-app-ingress
kubectl delete ingress challenge-app-ingress

# Lauch Deployment, Service and Ingress of challenge-app
kubectl apply -f ../k8s-challege-app
check_status $? "Error when launching kubernetes objects for challenge-app"

# Add challenge-app.info to /etc/HOSTS_ENTRY
echo "Setting minikube ip and host ($MINIKUBE_IP $HOSTS_ENTRY) to /etc/HOSTS_ENTRY"
sudo sh -c "echo '$MINIKUBE_IP $HOSTS_ENTRY' >> /etc/hosts"
check_status $? "Error adding challenge-app.info to /etc/HOSTS_ENTRY"

echo "The app should be running here: http://challenge-app.info/"