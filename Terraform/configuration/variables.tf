variable "region" {
  description = "Região da cloud em que será provisionada a instância"
}

variable "ssh-ips" {
  description = "IP ou range necessário para a liberação da porta SSH (192.168.1.4/32 ou 192.168.1.0/24)"
}

variable "amis" {
  type = "map"
  default = {
    "us-east-1" = "ami-0ff8a91507f77f867"
    "us-east-2" = "ami-0b59bfac6be064b78"
    "us-west-1" = "ami-0bdb828fd58c52235"
    "sa-east-1" = "ami-07b14488da8ea02a0"
    "eu-west-1" = "ami-047bb4163c506cd98"
  }
}