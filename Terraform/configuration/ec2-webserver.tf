# Create a new instance of the latest Amazon Linux AMI on an
# t2.micro node
provider "aws" {
  profile    = "default"
  region     = "${var.region}"
}

# Create security group with web and ssh access
resource "aws_security_group" "web_server" {
  name = "web_server"
  # SSH
  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["${var.ssh-ips}"]
  }
  # HTTP
  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP
  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Deploy ssh key for instance access
resource "aws_key_pair" "deployer" {
  key_name = "web_server" 
  public_key = "${file("~/.ssh/challenge_key.pub")}"
}

# Generate SSH keys
# resource "null_resource" "key_gen" {
#   provisioner "local-exec" {
#     command = "${fileexists("~/.ssh/challenge_key") == false ? "./create_ssh_key.sh" : "echo challenge_key already exists!"}"
#   }
# }

# Create web server
resource "aws_instance" "web_server" {
    ami = "${var.amis[var.region]}"
    vpc_security_group_ids = ["${aws_security_group.web_server.id}"]
    instance_type = "t2.micro"
    key_name      = "web_server"

    connection {
      user         = "ec2-user"
      private_key  = "${file("~/.ssh/challenge_key")}"
      host = "${self.public_ip}"
    }

    # Install Docker, run nginx container and get index.html in place
    provisioner "remote-exec" {
        inline = [
            "sudo chmod 777 /tmp/index.html",
            "sudo yum install -y docker",
            "sudo service docker start",
            "sudo docker pull nginx",
            "sudo docker run -d -p 80:80 -v /tmp:/usr/share/nginx/html --name nginx_test nginx",
        ]
    }

    # HTML file to be rendered by NGINX
    provisioner "file" {
        source = "index.html"
        destination = "/tmp/index.html"
    }

    
    # Save the public IP for testing
    provisioner "local-exec" {
      command = "echo ${aws_instance.web_server.public_ip} > public-ip.txt"
    }
}

